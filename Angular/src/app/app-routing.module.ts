import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { ReceitasComponent } from './receitas/receitas.component';
import { NewUserComponent } from './newuser/newuser.component';
import { NewReceitaComponent } from './newreceita/newreceita.component';
import { AuthGuard } from './guards/auth.guard';
import { MedicoGuard } from './guards/medico.guard';
import { UtenteGuard } from './guards/utente.guard';
import { NewPrescricaoComponent } from './newprescricao/newprescricao.component';
import { FarmaceuticoGuard } from './guards/farmaceutico.guard';
import { ReceitasUtenteComponent } from './receitas-utente/receitas-utente.component';
import { ApresentacaoMedsComponent } from './apresentacao-meds/apresentacao-meds.component';
import { ComentarioComponent } from './comentario/comentario.component';
import { DashboardUtenteComponent } from './dashboard-utente/dashboard-utente.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PostRegCodeComponent } from './post-reg-code/post-reg-code.component';
import { PostLogCodeComponent } from './post-log-code/post-log-code.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: NewUserComponent },
  { path: 'receitas', component: ReceitasComponent, canActivate: [AuthGuard, MedicoGuard] },
  { path: 'newreceita', component: NewReceitaComponent, canActivate: [AuthGuard, MedicoGuard] },
  { path: 'newprescricao/receita/:id', component: NewPrescricaoComponent, canActivate: [AuthGuard, MedicoGuard] },
  { path: 'comentar', component: ComentarioComponent, canActivate: [AuthGuard, MedicoGuard] },
  { path: 'dashboardm', component: DashboardComponent, canActivate: [AuthGuard, MedicoGuard] },
  { path: 'receitasU', component: ReceitasUtenteComponent, canActivate: [AuthGuard, UtenteGuard] },
  { path: 'dashboardutente', component: DashboardUtenteComponent, canActivate: [AuthGuard, UtenteGuard] },
  { path: 'apresentacoes', component: ApresentacaoMedsComponent},
  { path: 'regcode', component: PostRegCodeComponent},
  { path: 'logcode', component: PostLogCodeComponent},
  { path: 'dashboardU', component: DashboardUtenteComponent, canActivate: [AuthGuard, UtenteGuard]}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
