import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { ReceitasComponent } from './receitas/receitas.component';
import { LoginComponent } from './login/login.component';
import { ReceitasService } from './receitas.service';
import { AuthenticationService } from './authentication.service';
import { AuthGuard } from './guards/auth.guard';
import { MedicoGuard } from './guards/medico.guard';
import { UtenteGuard } from './guards/utente.guard';
import { FarmaceuticoGuard } from './guards/farmaceutico.guard';
import { NewUserComponent } from './newuser/newuser.component';
import { NewUserService } from './newuser.service';
import { NewReceitaComponent } from './newreceita/newreceita.component';
import { PrescricaoService } from './prescricao.service';
import { NewPrescricaoComponent } from './newprescricao/newprescricao.component';
import { ReceitasUtenteComponent } from './receitas-utente/receitas-utente.component';
import { ApresentacaoMedsComponent } from './apresentacao-meds/apresentacao-meds.component';
import { ApresentacaoService } from './apresentacao.service';
import { ComentarioComponent } from './comentario/comentario.component';
import { DashboardUtenteComponent } from './dashboard-utente/dashboard-utente.component';
import { Encryption } from './encryption/encryption';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PostRegCodeComponent } from './post-reg-code/post-reg-code.component';
import { CodeService } from './code.service';
import { PostLogCodeComponent } from './post-log-code/post-log-code.component';
import { GoogleAuthService } from "./google-auth.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  declarations: [
    AppComponent,
    ReceitasComponent,
    LoginComponent,
    NewUserComponent,
    NewReceitaComponent,
    NewPrescricaoComponent,
    ReceitasUtenteComponent,
    ApresentacaoMedsComponent,
    ComentarioComponent,
    DashboardUtenteComponent,
    DashboardComponent,
    PostRegCodeComponent,
    PostLogCodeComponent,
  ],
  providers: [ AuthGuard,
    MedicoGuard,
    UtenteGuard,
    FarmaceuticoGuard,
    AuthenticationService,
    ReceitasService,
    NewUserService,
    PrescricaoService,
    ApresentacaoService,
    CodeService,
    GoogleAuthService,
    Encryption
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
