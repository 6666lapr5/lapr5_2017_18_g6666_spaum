import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApresentacaoMedsComponent } from './apresentacao-meds.component';

describe('ApresentacaoMedsComponent', () => {
  let component: ApresentacaoMedsComponent;
  let fixture: ComponentFixture<ApresentacaoMedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApresentacaoMedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApresentacaoMedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
