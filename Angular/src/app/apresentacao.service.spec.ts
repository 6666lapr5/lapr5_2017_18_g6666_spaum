import { TestBed, inject } from '@angular/core/testing';

import { ApresentacaoMedsService } from './apresentacao.service';

describe('ApresentacaoMedsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApresentacaoMedsService]
    });
  });

  it('should be created', inject([ApresentacaoMedsService], (service: ApresentacaoMedsService) => {
    expect(service).toBeTruthy();
  }));
});
