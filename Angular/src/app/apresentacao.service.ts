import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Apresentacao } from './models/apresentacao';
import { AuthenticationService } from './authentication.service';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Comentario } from './models/comentario';

@Injectable()
export class ApresentacaoService {

  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { } 

  getApresentacoes(nome: string): Observable<Apresentacao[]> {
    const  apresentacoesUrl = `https://gdrlapr6666.azurewebsites.net/api/medicamento/nome=${nome}/apresentacoes`;
    
    return this.http.get<Apresentacao[]>(apresentacoesUrl).pipe(catchError(this.handleError<any>(`getApresentacoes medicamento=${nome}`)));
  }

  comentarApresentacao(nome: string, apresentacao_id: number, comentario: string): Observable<Comentario> {
    const url = `https://gdrlapr6666.azurewebsites.net/api/farmaco/nome=${nome}/apresentacao/${apresentacao_id}/comentar`;

    return this.http.post<Comentario>(url, { comentario: comentario }, this.getHeaders()).pipe(catchError(this.handleError<any>('comentarPrescricao', [])));
  }

  getHeaders() {
    let headers = new HttpHeaders({
      'x-access-token':
        this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }

  	/**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
