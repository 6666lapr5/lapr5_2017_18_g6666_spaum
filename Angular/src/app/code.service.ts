import { Injectable } from '@angular/core';

@Injectable()
export class CodeService {

  public code: string;
  public email: string;

  constructor() { }

  setCode(code: string) {
    this.code = code;
  }

  setEmail(email: string) {
    this.email = email;
  }

}
