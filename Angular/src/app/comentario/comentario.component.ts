import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PrescricaoService } from '../prescricao.service';
import { Apresentacao } from '../models/apresentacao';
import { Comentario } from '../models/comentario';
import { ApresentacaoService } from '../apresentacao.service';

@Component({
  selector: 'app-comentario',
  templateUrl: './comentario.component.html',
  styleUrls: ['./comentario.component.css']
})
export class ComentarioComponent implements OnInit {

  comentario: Comentario;

  constructor(
    private apresentacaoService: ApresentacaoService,
    //Angular service for interacting with the browser.
    private location: Location) { }

  ngOnInit() {
  }

  comentarApresentacao(nome: string, apresentacao_id: number, comentario: string) : void {
    this.apresentacaoService.comentarApresentacao(nome, apresentacao_id, comentario).subscribe(comentario => {
      this.comentario = comentario});
  }
}
