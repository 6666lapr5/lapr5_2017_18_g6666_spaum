import { Component, OnInit } from '@angular/core';
import { Receita } from '../models/receita';
import { ReceitasService } from '../receitas.service';
import { PrescricaoService } from '../prescricao.service';
import { Prescricao } from '../models/prescricao';
import { AuthenticationService } from '../authentication.service';
import { User } from '../models/user';

@Component({
  selector: 'app-dashboard-utente',
  templateUrl: './dashboard-utente.component.html',
  styleUrls: ['./dashboard-utente.component.css']
})
export class DashboardUtenteComponent implements OnInit {
  receitas: Receita[] = [];
  prescricao: Prescricao;
  prescricoes: Prescricao[];
  user: User;

  constructor(private receitaService: ReceitasService,
    private prescricaoService: PrescricaoService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {

    this.user = this.authenticationService.getUser();
    this.prescricaoService.getPrescricoesAviarUtente(this.user._id)
      .subscribe(prescricoes => {
        this.prescricoes = prescricoes;
      })      
  }

  getPrescricao(presc_id: string, receita_id: string){
    this.prescricaoService.getPrescricaoById(presc_id, receita_id)
    .subscribe(prescricao => {
      this.prescricao = prescricao;
    })
  }

}
