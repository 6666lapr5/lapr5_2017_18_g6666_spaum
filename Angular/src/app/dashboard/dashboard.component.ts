import { Component, OnInit } from '@angular/core';
import { ReceitasService } from '../receitas.service';
import { Receita } from '../models/receita';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  receitasTodas: Receita[] = [];
  receitas: Receita[] = [];

  constructor(private receitaService: ReceitasService) { }

  ngOnInit() {
    this.receitaService.getReceitas()
      .subscribe(receitasTodas => {
        this.receitasTodas = receitasTodas;
        if (this.receitasTodas.length <= 2) {
          this.receitas = this.receitasTodas;
        } else {
          for (var i = 0; i < this.receitasTodas.length; i++) {
            if(i== this.receitasTodas.length-1 || i== this.receitasTodas.length-2){
              this.receitas.push(this.receitasTodas[i]);
            }
          }
        }
      })

  }

}
