import { Injectable } from '@angular/core';
import { Encryption } from './encryption/encryption';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import * as jwt_decode from 'jwt-decode';
import { HttpClient, HttpHeaders, HttpErrorResponse } from
  '@angular/common/http';
import { CodeService } from './code.service';

@Injectable()
export class GoogleAuthService {

  private authUrl = 'https://gdrlapr6666.azurewebsites.net/api/authGoogle';

  constructor(
    private http: HttpClient,
    private encryption: Encryption,
    private codeService: CodeService) { }

  auth(code: string) : Observable<boolean> {
    return new Observable<boolean>(observer => {
      var encCode: string;
      encCode = this.encryption.encrypt(code).toString();

      var encEmail: string;
      encEmail = this.encryption.encrypt(this.codeService.email).toString();

      this.http.post<any>(this.authUrl, { email: encEmail, token: encCode}).subscribe(data => {
        if(data.success == true) {
          observer.next(true);
        } else {
          observer.next(false);
        }
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log("Client-side error occured.");
        } else {
          console.log("Server-side error occured.");
        }
        console.log(err);
        observer.next(false);
      });
    })
  }
}
