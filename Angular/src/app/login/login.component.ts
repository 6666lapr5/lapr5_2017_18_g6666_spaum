import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CodeService } from "../code.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  error = '';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private codeService: CodeService) { }

  ngOnInit() {
    this.authenticationService.logout();
    this.activatedRoute.params.subscribe(params => {
      if (params['u'] !== undefined) {
        ;
        this.error = 'O seu utilizador não pode aceder a receitas.';
      }
    });
  }
  login() {
    this.loading = true;
    this.authenticationService.login(this.model.email,
      this.model.password)
      .subscribe(result => {
        this.loading = false;
        if (result === true) {
          if(this.authenticationService.userInfo.utente){
            this.codeService.setEmail(this.model.email);
            this.router.navigate(['/logcode']);
          }

          if(this.authenticationService.userInfo.medico){
            this.router.navigate(['/dashboardm']);
          }
          
        } else {
          this.error = 'Nome de utilizador ou password incorretos.'
          alert(this.error);
        }
      });
  }
}
