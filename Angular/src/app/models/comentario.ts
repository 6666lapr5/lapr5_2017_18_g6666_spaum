export class Comentario {
    _id: string;
    forma: string;
    concentracao: string;
    quantidade: string;
    comentarios: string[];
}