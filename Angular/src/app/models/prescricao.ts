import { PosologiaPrescrita } from "./posologiaprescrita";

export class Prescricao {
    id: string;
    apresentacaoID: string;
    posologiaPrescrita: PosologiaPrescrita;
    validade: Date;
}