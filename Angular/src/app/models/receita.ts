import { Prescricao } from "./prescricao";

export class Receita {
    id: string;
    utente: string;
    medico: string;
    __v: string;
    prescricoes: String[];
}