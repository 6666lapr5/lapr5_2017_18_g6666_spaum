export class User {
    token: string;
    tokenExp: number;
    _id: string;
    medico: boolean;
    farmaceutico: boolean;
    utente: boolean;
}