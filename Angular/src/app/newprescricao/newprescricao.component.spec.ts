import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPrescricaoComponent } from './newprescricao.component';

describe('NewprescricaoComponent', () => {
  let component: NewPrescricaoComponent;
  let fixture: ComponentFixture<NewPrescricaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPrescricaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPrescricaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
