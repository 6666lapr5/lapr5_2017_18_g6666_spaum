import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Prescricao } from '../models/prescricao';
import { PrescricaoService } from '../prescricao.service';
import { Data } from '@angular/router/src/config';
import { ReceitasService } from '../receitas.service';
import { Receita } from '../models/receita';

@Component({
  selector: 'app-newprescricao',
  templateUrl: './newprescricao.component.html',
  styleUrls: ['./newprescricao.component.css']
})
export class NewPrescricaoComponent implements OnInit {
  prescricao: Prescricao;
  receita: Receita;
  model: any = {};
  loading = false;
  error = '';
  idRec: string;

  constructor(
    private route: ActivatedRoute,
    private receitasService: ReceitasService,
    private prescricaoService: PrescricaoService,
    private location: Location) { }

  ngOnInit() {
    this.idRec = this.route.snapshot.paramMap.get('id');
    this.receitasService.getReceitaByID(this.idRec)
      .subscribe(receita => this.receita = receita);
  }

  goBack(): void {
    this.location.back();
  }

  createPrescricao(apresentacao: number, quantidade: number, dias: number, intervalo: number, validade: Date): void {

    if (apresentacao.toString() == "" || quantidade.toString() == "" || dias.toString() == "" || intervalo.toString() || validade.toString() == "") {
      alert("Por favor preencha todos os campos")
    } else if (apresentacao <= 0) {
      alert("ID da apresentação tem de ser maior do que 0.")
    } else if (quantidade <= 0) {
      alert("Número de caixas tem de ser maior do que 0.")
    } else if (dias <= 0) {
      alert("Número de dias da toma tem de ser maior do que 0.")
    } else if (intervalo <= 0) {
      alert("Intervalo em horas tem de ser maior do que 0.")
    } else {
      this.prescricaoService.createPrescricao(this.receita.id, apresentacao, quantidade, dias, intervalo, validade)
        .subscribe(prescricao => {
          this.prescricao = prescricao;
        });
      alert("Prescrição criada com sucesso.")
    }
  }
}

