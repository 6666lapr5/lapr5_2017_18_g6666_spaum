import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewreceitaComponent } from './newreceita.component';

describe('NewreceitaComponent', () => {
  let component: NewreceitaComponent;
  let fixture: ComponentFixture<NewreceitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewreceitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewreceitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
