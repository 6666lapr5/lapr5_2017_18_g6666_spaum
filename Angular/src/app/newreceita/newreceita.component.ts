import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Receita } from '../models/receita';
import { ReceitasService } from '../receitas.service';

@Component({
  selector: 'app-newreceita',
  templateUrl: './newreceita.component.html',
  styleUrls: ['./newreceita.component.css'],
})
export class NewReceitaComponent implements OnInit {

  receitas: Receita[];
  model: any = {};
  loading = false;
  error = '';

  constructor(private receitaService: ReceitasService,
    //Angular service for interacting with the browser.
    private location: Location,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }

  createReceita(utente: string): void {
    utente = utente.trim();
    
    var prescricoes = [];

    if (!utente) {
      alert("Por favor insira o ID do utente.");
    } else {
      if (utente == this.authenticationService.userInfo._id) {
        alert("O utente inserido não é válido.")
      } else {
        this.receitaService.createReceita(utente)
          .subscribe(receita => {
            if(receita != null) {
              alert("Receita criada com sucesso.");
              this.receitas.push(receita);              
            } else {
              alert("Receita não pôde ser criada.");              
            }
          });
      }
    }
  }
}
