import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import * as jwt_decode from 'jwt-decode';
import { AuthenticationService } from './authentication.service';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from '../app/models/user';
import { Code } from '../app/models/code';
import { of } from 'rxjs/observable/of';

@Injectable()
export class NewUserService {

  private urlCreate = 'https://gdrlapr6666.azurewebsites.net/api/user';

  constructor(private http: HttpClient) { }

  /** POST: create the user on the server */
  createUser(name: string, password: string, email:string, facebook: number): Observable<Code> {

    return this.http.post<Code>(this.urlCreate, {name: name, password: password, email: email, utente: true, farmaceutico: false, medico: false, facebook: facebook}).pipe(catchError(this.handleError<any>('createUser'))
    );
  }

  getHeaders() {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }

	/**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}