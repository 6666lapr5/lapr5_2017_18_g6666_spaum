import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { NewUserService } from '../newuser.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { User } from '../models/user';
import { CodeService } from "../code.service";

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class NewUserComponent implements OnInit {

  model: any = {};
  loading = false;
  error = '';

  constructor(private newUserService: NewUserService,
    //Angular service for interacting with the browser.
    private location: Location,
    private router: Router,
    private codeService: CodeService) { }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }

  createUser(nome: string, password: string, email: string, facebook: number): void {
		this.newUserService.createUser(nome, password, email, facebook)
			.subscribe(code => {

        if(code) {
          alert("Utilizador criado com sucesso.")
          this.codeService.setCode(code.googlekey);
          this.router.navigate(['/regcode']);
        } else {
          alert("Utilizador não pôde ser criado.")
        }
			});

  }
  
}

