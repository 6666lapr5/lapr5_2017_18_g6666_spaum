import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogCodeComponent } from './post-log-code.component';

describe('LogCodeComponent', () => {
  let component: LogCodeComponent;
  let fixture: ComponentFixture<LogCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
