import { Component, OnInit } from '@angular/core';
import { GoogleAuthService } from "../google-auth.service";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post-log-code',
  templateUrl: './post-log-code.component.html',
  styleUrls: ['./post-log-code.component.css']
})
export class PostLogCodeComponent implements OnInit {

  constructor(private googleAuthService: GoogleAuthService,
  private router: Router) { }

  ngOnInit() {
  }

  enviarCodigoGoogle(code: string) {
    this.googleAuthService.auth(code).subscribe(result => {
      if(result) {
        this.router.navigate(['/dashboardutente']);
      } else {
        alert('Código errado!');
      }
    });
  }
}
