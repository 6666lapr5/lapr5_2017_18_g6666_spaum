import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostRegCodeComponent } from './post-reg-code.component';

describe('PostRegCodeComponent', () => {
  let component: PostRegCodeComponent;
  let fixture: ComponentFixture<PostRegCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostRegCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostRegCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
