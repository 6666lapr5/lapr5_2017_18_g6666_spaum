import { Component, OnInit } from '@angular/core';
import { CodeService } from "../code.service";

@Component({
  selector: 'app-post-reg-code',
  templateUrl: './post-reg-code.component.html',
  styleUrls: ['./post-reg-code.component.css']
})
export class PostRegCodeComponent implements OnInit {

  private code: string;

  constructor(private codeService: CodeService) { }

  ngOnInit() {
    this.code = this.codeService.code;
  }
}
