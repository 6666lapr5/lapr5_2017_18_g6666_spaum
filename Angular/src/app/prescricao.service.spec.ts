import { TestBed, inject } from '@angular/core/testing';

import { PrescricaoService } from './prescricao.service';

describe('PrescricaoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrescricaoService]
    });
  });

  it('should be created', inject([PrescricaoService], (service: PrescricaoService) => {
    expect(service).toBeTruthy();
  }));
});
