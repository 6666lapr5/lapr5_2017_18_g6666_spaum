import { Injectable } from '@angular/core';
import { Receita } from '../app/models/receita';
import { Prescricao } from '../app/models/prescricao';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class PrescricaoService {

  constructor(private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  getPrescricaoById(presc_id: string, receita_id: string): Observable<Prescricao> {
    const url = `https://gdrlapr6666.azurewebsites.net/api/receita/${receita_id}/prescricao/${presc_id}`;
    return this.http.get<Prescricao>(url, this.getHeaders()).pipe(catchError(this.handleError<any>(`getPrescricaoById receitaID=${receita_id} & prescricaoID=${presc_id}`)));
  }

  getPrescricoesAviarUtente(idUtente: string): Observable<Prescricao[]> {
    const url = `https://gdrlapr6666.azurewebsites.net/api/Utente/${idUtente}/prescricao/poraviar`;
    return this.http.get<Prescricao[]>(url, this.getHeaders()).pipe(catchError(this.handleError<Prescricao[]>(`getPrescricoesAviarUtente`)));
  }

  //POST prescricao
  createPrescricao(receita: string, apresentacao: number, nCaixas: number, dias: number, intervalo: number, validade: Date): Observable<Prescricao> {
    const prescricaoUrl = `https://gdrlapr6666.azurewebsites.net/api/Receita/${receita}/prescricao`;
    
    return this.http.post<Prescricao>(prescricaoUrl, {
      apresentacao: apresentacao,
      posologiaPrescrita: { nCaixas: nCaixas, dias: dias, intervalo: intervalo },
      validade: validade
    }, this.getHeaders()).pipe(catchError(this.handleError<Prescricao>('createPrescricao'))
    );
  }

  getHeaders() {
    let headers = new HttpHeaders({
      'x-access-token':
        this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

