import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceitasUtenteComponent } from './receitas-utente.component';

describe('ReceitasUtenteComponent', () => {
  let component: ReceitasUtenteComponent;
  let fixture: ComponentFixture<ReceitasUtenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceitasUtenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceitasUtenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
