import { Component, OnInit } from '@angular/core';
import { Receita } from '../models/receita';
import { ReceitasService } from '../receitas.service';
import { PrescricaoService } from '../prescricao.service';
import { Prescricao } from '../models/prescricao';

@Component({
  selector: 'app-receitas-utente',
  templateUrl: './receitas-utente.component.html',
  styleUrls: ['./receitas-utente.component.css']
})
export class ReceitasUtenteComponent implements OnInit {

  receitas: Receita[] = [];
  prescricao: Prescricao;

  constructor(private receitaService: ReceitasService,
  private prescricaoService: PrescricaoService) { }

  ngOnInit() {
    
    this.receitaService.getReceitas()
      .subscribe(receitas => {
        this.receitas = receitas;
      })
  }

  getPrescricao(presc_id: string, receita_id: string){
    this.prescricaoService.getPrescricaoById(presc_id, receita_id)
    .subscribe(prescricao => {
      this.prescricao = prescricao;
    })
  }
}
