import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Receita } from './models/receita';
import { AuthenticationService } from './authentication.service';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable()
export class ReceitasService {

  private receitasUrl = 'https://gdrlapr6666.azurewebsites.net/api/receita';
  //verificar o servidor:porta
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  getReceitas(): Observable<Receita[]> {
    return this.http.get<Receita[]>(this.receitasUrl, this.getHeaders());
  }

  getReceitaByID(receita_id: string): Observable<Receita> {
    return this.http.get<Receita>(`http://localhost:8443/api/receita/${receita_id}`, this.getHeaders());
  }

  // POST: cria a receita
  createReceita(utente: string): Observable<Receita> {
    return this.http.post<Receita>(this.receitasUrl, {utente: utente}, this.getHeaders()).pipe(catchError(this.handleError<Receita>('createReceita'))
    );
  }

  getHeaders() {
    let headers = new HttpHeaders({
      'x-access-token':
        this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
