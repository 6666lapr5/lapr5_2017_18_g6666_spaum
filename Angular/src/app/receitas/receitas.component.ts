import { Component, OnInit } from '@angular/core';
import { Receita } from '../models/receita';
import { ReceitasService } from '../receitas.service';
import { PrescricaoService } from '../prescricao.service';
import { Prescricao } from '../models/prescricao';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-receitas',
  templateUrl: './receitas.component.html',
  styleUrls: ['./receitas.component.css']
})
export class ReceitasComponent implements OnInit {
  receita: Receita;
  receitas: Receita[] = [];
  prescricao: Prescricao;
  show: Boolean;
  rec_id: string;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private receitasService: ReceitasService,
    private prescricaoService: PrescricaoService) { }

  ngOnInit() {
    this.receitasService.getReceitas()
      .subscribe(receitas => {
        this.receitas = receitas;
        this.show = false;
      })
  }

  getPrescricao(presc_id: string, receita_id: string) {
    this.prescricaoService.getPrescricaoById(presc_id, receita_id)
      .subscribe(prescricao => {
        this.prescricao = prescricao;
      })
  }

  setReceita(rec: Receita) {//o que achas?
    this.rec_id = rec.id;
    this.show = true;
  }

  adicionarPrescricao(){
    console.log('OLAAAA  '+ this.rec_id)
    this.router.navigate(['/newprescricao/receita/'+this.rec_id]);
  }
}
